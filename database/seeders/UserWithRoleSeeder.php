<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserWithRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = new User;
        $admin->name = 'Administrator';
        $admin->email = 'admin@bumi.com';
        $admin->email_verified_at = date('Y-m-d H:i:s');
        $admin->password = bcrypt('Admin.bumi');
        $admin->role = 'admin';
        $admin->save();

        $admin = new User;
        $admin->name = 'M Tri Andika';
        $admin->email = 'andika.kurniawan@bakrie.ac.id';
        $admin->email_verified_at = date('Y-m-d H:i:s');
        $admin->password = bcrypt('UBakrie.12920.001');
        $admin->role = 'manager';
        $admin->save();

        $admin = new User;
        $admin->name = 'Aditya Gunawan';
        $admin->email = 'aditya.gunawan@bakrie.ac.id';
        $admin->email_verified_at = date('Y-m-d H:i:s');
        $admin->password = bcrypt('UBakrie.12920.002');
        $admin->role = 'manager';
        $admin->save();

        $admin = new User;
        $admin->name = 'Yudha Kurniawan';
        $admin->email = 'yudha.kurniawan@bakrie.ac.id';
        $admin->email_verified_at = date('Y-m-d H:i:s');
        $admin->password = bcrypt('UBakrie.12920.003');
        $admin->role = 'manager';
        $admin->save();

        $admin = new User;
        $admin->name = 'Jacob Wijaya';
        $admin->email = 'jacob.wijaya@bakrie.ac.id';
        $admin->email_verified_at = date('Y-m-d H:i:s');
        $admin->password = bcrypt('sabakuseptian553');
        $admin->role = 'manager';
        $admin->save();
    }
}