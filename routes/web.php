<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get('/tables', function () {
    return view('tables');
});

Route::get('/dashboard', function () {
    return view('dashboard');
});

Route::get('/profile', function () {
    return view('profile');
});

Route::get('/rtl', function () {
    return view('rtl');
});

Route::get('/virtual-reality', function () {
    return view('virtual-reality');
});

Route::get('/billing', function () {
    return view('billing');
});

Route::get('/mediasocial', function () {
    return view('mediasocial');
});

Route::get('/percakapan', function () {
    return view('percakapan');
});

Route::get('/migor', function () {
    return view('migor');
});

Route::get('/borobudur', function () {
    return view('borobudur');
});

Route::get('/homes1', function () {
    return view('layouts/homes1');
});

Route::get('/teks', function () {
    return view('teks');
});

Route::get('/fokus', function () {
    return view('fokus');
});

Route::get('/kebijakan', function () {
    return view('kebijakan');
});

Route::get('/kunjungan', function () {
    return view('kunjungan');
});

Route::get('/reformasi', function () {
    return view('reformasi');
});

Route::get('/wrapper', function () {
    return view('wrapper');
});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::get('/news', 'ApiController@displayNews');
Route::post('/sourceId', 'ApiController@displayNews');


