<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Central Data</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="css/plyr.css" type="text/css">
    <link rel="stylesheet" href="css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    <header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="./index.html">
                            <img src="/resizelogo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li><a href="/home">Homepage</a></li>
                                <!-- <li><a href="./categories.html">Categories <span class="arrow_carrot-down"></span></a> -->
                                    <!-- <ul class="dropdown">
                                        <li><a href="./categories.html">Categories</a></li>
                                        <li><a href="./anime-details.html">Anime Details</a></li>
                                        <li><a href="./anime-watching.html">Anime Watching</a></li>
                                        <li><a href="./blog-details.html">Blog Details</a></li>
                                        <li><a href="./signup.html">Sign Up</a></li>
                                        <li><a href="./login.html">Login</a></li>
                                    </ul> -->
                                </li>
                                <li><a href="#">News</a></li>
                             
                            </ul>
                        </nav>
                    </div>
                </div>
               
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="{{ route('logout') }}"><span class="fa fa-sign-out"></span>  Sign Out</a>
                    </div>
                </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
    <!-- Header End -->

    <!-- Breadcrumb Begin -->
    <div class="breadcrumb-option">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb__links">
                        <a href="./index.html"><i class="fa fa-home"></i> Home</a>
                        <a href="./categories.html">Categories</a>
                        <span>Author</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Anime Section Begin -->
    <section class="anime-details spad">
        <div class="container">
            <div class="anime__details__content">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="anime__details__pic set-bg" data-setbg="/assets/andika.jpeg">
                            
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="anime__details__text">
                            <div class="anime__details__title">
                                <h3>Muhammad Tri Andika Kurniawan, S.Sos, M.A</h3>
                                <span>Dosen Program Studi Ilmu Politik</span>
                            </div>
                            <div class="anime__details__rating">
                                <div class="rating">
                                    
                                </div>
                                
                            </div>
                            <p>Muhammad Tri Andika memperoleh gelar sarjana dari Departemen Ilmu Politik Universitas Indonesia. Tamat dari Universitas Indonesia, Andika mengikuti short course di Departemen Sosiologi Nanyang Technological University, Singapura selama enam bulan pada tahun 2008. Pada tahun 2008-2010, Andika melanjutkan studi Master di Graduate School of International Relations, Ritsumeikan University Jepang. 2010-2011, Andika juga berkesempatan menjadi Asisten Profesor untuk mata kuliah International Relations di Ritsumeikan University.</p><br><br>
            
                            <!-- <div class="anime__details__widget">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <ul>
                                            <li><span>Type:</span> TV Series</li>
                                            <li><span>Studios:</span> Lerche</li>
                                            <li><span>Date aired:</span> Oct 02, 2019 to ?</li>
                                            <li><span>Status:</span> Airing</li>
                                            <li><span>Genre:</span> Action, Adventure, Fantasy, Magic</li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <ul>
                                            <li><span>Scores:</span> 7.31 / 1,515</li>
                                            <li><span>Rating:</span> 8.5 / 161 times</li>
                                            <li><span>Duration:</span> 24 min/ep</li>
                                            <li><span>Quality:</span> HD</li>
                                            <li><span>Views:</span> 131,541</li>
                                        </ul>
                                    </div>
                                </div>
                            </div> -->
                            <div class="anime__details__btn">
                                <a href="#" class="follow-btn"><i class="fa fa-heart-o"></i> Follow</a>
                                <a href="#" class="watch-btn"><span>Research</span> <i
                                    class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <br><br><br>
                                    <br>
                        <div class="anime__details__pic set-bg" data-setbg="/picture1.jpg">
                            
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="anime__details__text">
                            <div class="anime__details__title">
                                <br>
                                <br>
                                <br>
                                <br><br>
                                <h3>Dr.rer.pol. Aditya Batara Gunawan, S. Sos., M. Litt</h3>
                                <span>Dosen Program Studi Ilmu Politik</span>
                            </div>
                            <div class="anime__details__rating">
                                <div class="rating">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    
                                </div>
                                
                            </div>
                            <p>Aditya memperoleh gelar sarjana (S. Sos) dari Program Studi S1 Ilmu Politik Universitas Indonesia dan melanjutkan studi magister kajian perdamaian (M. Litt) di University of St Andrews, UK. Kemudian, Aditya mendapatkan Gelar doktor dalam bidang ilmu politik (Doctor Rerum Politicarum/Dr.rer.pol) dari Heidelberg University, Jerman. Aditya memulai karirnya sebagai peneliti di sebuah NGO yang memfokuskan pada isu-isu-pertahanan, dan kemudian dipercaya menjadi staf ahli pimpinan Komisi I DPR RI. Pada tahun 2011, Aditya bergabung dengan Universitas Bakrie sebagai dosen tetap Prodi Ilmu Politik. Minat kajiannya adalah politik pertahanan, politik legislasi, resolusi konflik, dan computational social science.</p><br><br>
                            <div class="anime__details__btn">
                                <a href="#" class="follow-btn"><i class="fa fa-heart-o"></i> Follow</a>
                                <a href="#" class="watch-btn"><span>Research</span> <i
                                    class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                    </div>
                    <div class="col-lg-3">
                            <br><br><br>
                                    <br>
                        <div class="anime__details__pic set-bg" data-setbg="/picture2.jpg">
                            
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <div class="anime__details__text">
                            <div class="anime__details__title">
                                <br>
                                <br>
                                <br>
                                <br><br>
                                <h3>Yudha Kurniawan S.Sos., M.A</h3>
                                <span>Dosen Program Studi Ilmu Politik</span>
                            </div>
                            <div class="anime__details__rating">
                                <div class="rating">
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    
                                </div>
                                
                            </div>
                            <p>Yudha memperoleh gelar Sarjana (S.Sos) dari Program Studi S1 Hubungan Internasional Universitas Prof. Dr. Moestopo (Beragama) Jakarta dan melanjutkan studi S2 (M.A) Ilmu Politik (Konsentrasi Hubungan Internasional) di Universitas Gadjah Mada, Yogyakarta. Yudha memulai karirnya sebagai peneliti pada NGO yang memiliki fokus pada kajian strategis dan pertahanan. Pada tahun 2020, Yudha bergabung dengan Universitas Bakrie sebagai dosen Program Studi Ilmu Politik. Yudha saat ini memiliki fokus pada kajian hubungan internasional, keamanan internasional, dan isu-isu global kontemporer.</p><br><br>
                            <div class="anime__details__btn">
                                <a href="#" class="follow-btn"><i class="fa fa-heart-o"></i> Follow</a>
                                <a href="#" class="watch-btn"><span>Research</span> <i
                                    class="fa fa-angle-right"></i></a>
                                </div>
                            </div>
                    </div>
                </div>
                </div>
                

                



                <!-- <div class="row">
                    <div class="col-lg-8 col-md-8">
                        <div class="anime__details__review">
                            <div class="section-title">
                                <h5>Reviews</h5>
                            </div>
                            <div class="anime__review__item">
                                <div class="anime__review__item__pic">
                                    <img src="img/anime/review-1.jpg" alt="">
                                </div>
                                <div class="anime__review__item__text">
                                    <h6>Chris Curry - <span>1 Hour ago</span></h6>
                                    <p>whachikan Just noticed that someone categorized this as belonging to the genre
                                    "demons" LOL</p>
                                </div>
                            </div>
                            <div class="anime__review__item">
                                <div class="anime__review__item__pic">
                                    <img src="img/anime/review-2.jpg" alt="">
                                </div>
                                <div class="anime__review__item__text">
                                    <h6>Lewis Mann - <span>5 Hour ago</span></h6>
                                    <p>Finally it came out ages ago</p>
                                </div>
                            </div>
                            <div class="anime__review__item">
                                <div class="anime__review__item__pic">
                                    <img src="img/anime/review-3.jpg" alt="">
                                </div>
                                <div class="anime__review__item__text">
                                    <h6>Louis Tyler - <span>20 Hour ago</span></h6>
                                    <p>Where is the episode 15 ? Slow update! Tch</p>
                                </div>
                            </div>
                            <div class="anime__review__item">
                                <div class="anime__review__item__pic">
                                    <img src="img/anime/review-4.jpg" alt="">
                                </div>
                                <div class="anime__review__item__text">
                                    <h6>Chris Curry - <span>1 Hour ago</span></h6>
                                    <p>whachikan Just noticed that someone categorized this as belonging to the genre
                                    "demons" LOL</p>
                                </div>
                            </div>
                            <div class="anime__review__item">
                                <div class="anime__review__item__pic">
                                    <img src="img/anime/review-5.jpg" alt="">
                                </div>
                                <div class="anime__review__item__text">
                                    <h6>Lewis Mann - <span>5 Hour ago</span></h6>
                                    <p>Finally it came out ages ago</p>
                                </div>
                            </div>
                            <div class="anime__review__item">
                                <div class="anime__review__item__pic">
                                    <img src="img/anime/review-6.jpg" alt="">
                                </div>
                                <div class="anime__review__item__text">
                                    <h6>Louis Tyler - <span>20 Hour ago</span></h6>
                                    <p>Where is the episode 15 ? Slow update! Tch</p>
                                </div>
                            </div>
                        </div>
                        <div class="anime__details__form">
                            <div class="section-title">
                                <h5>Your Comment</h5>
                            </div>
                            <form action="#">
                                <textarea placeholder="Your Comment"></textarea>
                                <button type="submit"><i class="fa fa-location-arrow"></i> Review</button>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-8 col-sm-8">
                    <div class="sidebar_product">
                        <div class="product__sidebar__view">
                            <div class="section-title">
                                <h5>Main Menu</h5>
                            </div>
                            <!-- <ul class="filter__controls">
                                <li class="active" data-filter="*">Survey</li>
                                <li data-filter=".week">Analisis</li>
                                <li data-filter=".month">Actor Mapping </li>
                                <li data-filter=".years">Media Monitoring</li>
                            </ul> -->
                            <!-- <div class="coloumn">
                                <div class = "product__sidebar__view__item set-bg mix month week" data-setbg="/assets/2.jpg">
                                    <h5><a href="/billing">Survey</a></h5>
                                </div>
                                 <div class="product__sidebar__view__item set-bg mix month week" data-setbg="/assets/1.jpg">
                                    <h5><a href="#">Actor Mapping</a></h5>
                                </div>
                                 <div class="product__sidebar__view__item set-bg mix week years" data-setbg="/assets/4.jpg">
                                    <h5><a href="#">Analysis</a></h5>
                                </div>
                                <div class="product__sidebar__view__item set-bg mix years month" data-setbg="/assets/4.jpg">
                                    <h5><a href="#">Analysis</a></h5>
                                </div>
                                <div class="product__sidebar__view__item set-bg mix years month" data-setbg="/assets/3.jpg">
                                    <h5><a href="#">Media Monitoring</a></h5>
                                </div>
                            </div>  
                        </div>         -->
                    </div>
                </div> -->
            </div>
        </section>
        <!-- Anime Section End -->

        <!-- Footer Section Begin -->
        <footer class="footer">
            <div class="page-up">
                <a href="#" id="scrollToTopButton"><span class="arrow_carrot-up"></span></a>
                
            </div>
            <span>Program Studi Ilmu Politik Universitas Bakrie</span> 
           
            <div class="container">
                <div class="row">
                    <div class="col-lg-3">
                        <div class="footer__logo">
                            <a href="#"><img src="/logo1.png" alt=""></a>
                        </div>
                    </div>
                    <!-- <div class="col-lg-6">
                        <div class="footer__nav">
                            <ul>
                                <li class="active"><a href="./index.html">Homepage</a></li>
                                <li><a href="./categories.html">Categories</a></li>
                                <li><a href="./blog.html">Our Blog</a></li>
                                <li><a href="#">Contacts</a></li>
                            </ul>
                        </div>
                    </div> -->

              </div>
          </footer>
          <!-- Footer Section End -->

          <!-- Search model Begin -->
          <div class="search-model">
            <div class="h-100 d-flex align-items-center justify-content-center">
                <div class="search-close-switch"><i class="icon_close"></i></div>
                <form class="search-model-form">
                    <input type="text" id="search-input" placeholder="Search here.....">
                </form>
            </div>
        </div>
        <!-- Search model end -->

        <!-- Js Plugins -->
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/player.js"></script>
        <script src="js/jquery.nice-select.min.js"></script>
        <script src="js/mixitup.min.js"></script>
        <script src="js/jquery.slicknav.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/main.js"></script>

    </body>

    </html>