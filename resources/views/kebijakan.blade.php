<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Center - Universitas Bakrie</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/plyr.css" type="text/css">
    <link rel="stylesheet" href="/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/home">
                            <img src="/resizelogo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="home">Homepage</a></li>
                                <!-- <li><a href="/categories.html">Categories <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="./categories.html">Bisnis</a></li>
                                        <li><a href="./anime-details.html">Keuangan</a></li>
                                        <li><a href="./anime-watching.html">Teknologi</a></li>
                                        <li><a href="./blog-details.html">Politik</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li><a href="http://localhost/central-data/news-app/public/">News</a></li> -->
                                <li><a href="./profile">Profil</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="{{ route('logout') }}"><span class="fa fa-sign-out"></span>  Sign Out</a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
<section class="product spad">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="sidebar_product">
                        <div class="product__sidebar__view">
                            <div class="section-title">
                                <h5>Arah kebijakan luar negeri Indonesia di tahun 2022</h5>
                            </div>
                            <p>Menteri Luar Negeri Retno Marsudi menyampaikan Pernyataan Pers Tahunan Menteri Luar Negeri (PPTM) pada Hari Kamis (06/01) lalu. Kegiatan PPTM merupakan sebuah event penyampaian pokok-pokok kebijakan strategis pemerintah di sektor diplomasi kepada publik dan menjadi referensi bagi arah kebijakan luar negeri Indonesia secara tahunan. Tim CPBI melakukan analisis teks kuantitatif terhadap 2971 kata yang disampaikan oleh Menlu Retno di kegiatan PPTM tersebut. </p><br>
                            <img src="/a3.png"></img><br><br>
                            <p>Dari hasil analisis yang dilakukan, Pemerintah melalui kementerian luar negeri nampaknya masih akan memprioritaskan pada isu kesehatan di tingkat global, hal ini terlihat dari masih tingginya frekuensi penggunaan kata-kata seperti “kesehatan”, “vaksin”, dan “pandemi” (Gambar 1). Selain itu masuknya kata “asean” dalam daftar kata teratas menunjukkan bahwa Kemenlu masih menempatkan dinamika politik luar negeri di kawasan ASEAN sebagai lingkaran konsentris diplomasi yang utama. </p>
                            <br>
                            <img src="/a4.png"></img>
                            <br><br>
                            <p>Terkait ASEAN, kemunculan kata “Myanmar” yang cukup sering, menunjukkan bahwa krisis politik di Myanmar setelah kudeta pasca pemilu akan terus menjadi perhatian diplomat Indonesia (Grafik 1). Pada tahun 2021 yang lalu, Indonesia dan negara-negara ASEAN lainnya berhasil untuk tidak mengikutsertakan pemimpin junta Myanmar Min Aung Hlaing dalam KTT ASEAN. Menariknya, dalam pidatonya, Menlu Retno memberikan porsi yang cukup tinggi untuk kata “afghanistan”. Hal ini mengisyaratkan bahwa Kemenlu akan lebih banyak berpartisipasi di tingkat global dalam isu-isu terkait afghanistan pasca dikuasai oleh Taliban. Pada akhir tahun lalu, Menlu Retno telah menemui perwakilan pihak Taliban, Amir Khan Muttaqi, di sela Konferensi Tingkat Menteri Organisasi Kerja sama Islam (OKI) di Islamabad, Pakistan. Beberapa agenda yang menjadi pembahasan di pertemuan tersebut diantaranya adalah situasi kemanusiaan di Afghanistan, pendidikan dan pemberdayaan perempuan. </p>
                        </div>        
                    </div>
                </div>
        </div>
    </div>

  <!-- Footer Section End -->