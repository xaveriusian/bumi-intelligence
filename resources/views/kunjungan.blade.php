<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Center - Universitas Bakrie</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/plyr.css" type="text/css">
    <link rel="stylesheet" href="/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/home">
                            <img src="/.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="home">Homepage</a></li>
                                <!-- <li><a href="/categories.html">Categories <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="./categories.html">Bisnis</a></li>
                                        <li><a href="./anime-details.html">Keuangan</a></li>
                                        <li><a href="./anime-watching.html">Teknologi</a></li>
                                        <li><a href="./blog-details.html">Politik</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li><a href="http://localhost/central-data/news-app/public/">News</a></li> -->
                                <li><a href="./profile">Profil</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="{{ route('logout') }}"><span class="fa fa-sign-out"></span>  Sign Out</a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
<section class="product spad">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="sidebar_product">
                        <div class="product__sidebar__view">
                            <div class="section-title">
                                <h5>Kunjungan Perdana Menteri Australia Anthony Albanese ke Indonesia</h5>
                            </div>
                            <p>Perdana Menteri Australia Anthony Albanese yang baru terpilih, memulai kunjungan kenegaraan resmi pertama kali ke Indonesia pada hari Minggu (05/06) lalu. Pada hari Senin (06/06), PM Anthony mengadakan pertemuan dengan Presiden Joko Widodo (Jokowi) di Istana Kepresidenan Bogor. Setelah pertemuan, keduanya mengadakan pernyataan pers secara bersama-bersama. Tim CPBI melakukan analisis teks kuantitatif terhadap pernyataan pers yang diberikan oleh kedua pemimpin negara tersebut. </p><br>
                            <img src="/a11.png"></img><br><br>
                            <p>Presiden Jokowi dalam pernyataanya lebih banyak menggunakan beberapa kata seperti kerja, bilateral, kawasan, kemitraan, investasi, dan ekspor (Gambar 1). Hal ini mengindikasikan harapan pemerintah Indonesia mengenai penguatan kerjasama ekonomi antara kedua negara, terutama terkait IA-CEPA (The Indonesia–Australia Comprehensive Economic Partnership Agreement), yang juga menjadi kata dengan frekuensi tertinggi dalam pernyataan presiden. </p>
                            <br>
                            <img src="/a12.png"></img><br>
                            <br>
                            <p>Selain itu, kata “iklim” dan “energi” juga menempati frekuensi yang cukup sering dalam pernyataan Presiden Jokowi (Grafik 1). Kata tersebut mengisyaratkan adanya prioritas kerjasama di sektor energi dan perubahan iklim antara Indonesia dan Australia. </p>
                            <img src="a13.png"></img><br><br>
                            <p>Tidak jauh berbeda, PM Anthony memberikan penekanan pada kata-kata seperti economy, relationship, cooperation, dan trade yang menunjukkan harapan pemerintahannya akan keberlanjutan kerjasama ekonomi dengan Indonesia (Gambar 2). Secara umum, terdapat tiga klaster isu yang dapat dikelompokkan dan menjadi perhatian PM Anthony jika ditinjau dari frekuensi penggunaan kata yang diucapkan (Grafik 2).  </p>
                            <img src="a14.png"></img><br><br>
                            <p>Dalam klaster pertama, penggunaan kata asean, indo-pacific, dan region mengindikasikan adanya ketertarikan pemerintah Australia pada aspek kerjasama yang lebih luas namun melibatkan Indonesia di tingkat ASEAN dan kawasan indo-pasifik. Untuk klaster kedua, PM Anthony juga banyak menggunakan kata energy dan climate dalam pernyataannya, yang menunjukkan fokus pemerintah Australia kedepan terkait kerjasama dengan Indonesia pada isu-isu terkait energi dan perubahan iklim. Terakhir, di klaster ketiga, PM Anthony banyak menggunakan istilah secure dan security. Hal ini menunjukkan bahwa Australia dibawah kepemimpinannya masih akan melanjutkan fokus pada isu-isu keamanan terkait hubungan diplomatiknya dengan Indonesia. </p>
                        </div>        
                    </div>
                </div>
        </div>
    </div>

  <!-- Footer Section End -->