<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Center - Universitas Bakrie</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/plyr.css" type="text/css">
    <link rel="stylesheet" href="/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/home">
                            <img src="/cpbixbumi.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="home">Homepage</a></li>
                                <!-- <li><a href="/categories.html">Categories <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="./categories.html">Bisnis</a></li>
                                        <li><a href="./anime-details.html">Keuangan</a></li>
                                        <li><a href="./anime-watching.html">Teknologi</a></li>
                                        <li><a href="./blog-details.html">Politik</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li><a href="http://localhost/central-data/news-app/public/">News</a></li> -->
                                <li><a href="./profile">Profil</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="{{ route('logout') }}"><span class="fa fa-sign-out"></span>  Sign Out</a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
<section class="product spad">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="sidebar_product">
                        <div class="product__sidebar__view">
                            <div class="section-title">
                                <h5>Fokus pemerintah untuk RAPBN 2023: Pemulihan Ekonomi dan antisipasi krisis global.</h5>
                            </div>
                            <p>Pada Hari Jumat (20/05) kemarin, Menteri Keuangan (Menkeu) Sri Mulyani Indrawati menyampaikan pidato mengenai Pengantar dan Keterangan Pemerintah atas Kerangka Ekonomi Makro dan Pokok-Pokok Kebijakan Fiskal (KEM-PPKF) TA 2023 <br> dalam Rapat Paripurna DPR RI ke-18. Tim CPBI U-Bakrie melakukan analisis teks kuantitatif terhadap pidato yang disampaikan Menteri Sri Mulyani untuk memetakan apa saja yang menjadi concern pemerintah untuk tahun anggaran 2023. 
Berdasarkan pengolahan data teks yang dilakukan terhadap penggunaan 3260 kata dalam pidato tersebut, tim CPBI menemukan bahwa 10 kata teratas yang paling sering digunakan oleh Menteri Sri Mulyani adalah: ekonomi; kebijakan; fiskal; pemulihan; Indonesia; global; mendorong; pandemi, dan; pembangunan (Gambar 1). Hal ini menunjukkan bahwa pemerintah masih berfokus pada pemulihan ekonomi negara dalam konteks pandemi untuk TA 2023.<br>
</p><br>
                            <img src="/a3.png"></img><br><br>
                            <p>Beberapa kata lainnya yang juga sering digunakan pemerintah adalah reformasi, inflasi, krisis, dan kenaikan. Pilihan kata reformasi menunjukan keselarasan dengan hasil analisis terhadap pidato Presiden Kemudian, kata-kata seperti inflasi, krisis, dan kenaikan mengisyaratkan bahwa adanya kesadaran dari pihak pemerintah mengenai ancaman krisis ekonomi global di tahun 2023 yang berupa inflasi dan kenaikan harga seperti diprediksi oleh berbagai analis ekonomi. <p>
</p>
                            
                            <img src="/a4.png"></img>
                            <br><br>
                            <p>Selain itu, tim peneliti juga menemukan bahwa kata “UU (Undang-Undang)” menempati posisi terakhir dalam frekuensi kata yang muncul (Grafik 1). Hal ini mengisyaratkan bahwa pemerintah tidak begitu menempatkan perhatian yang besar pada penataan regulasi terkait pemulihan ekonomi.  Menariknya, kata “pangan” juga berada dalam daftar 10 kata paling jarang digunakan, padahal persoalan stok pangan global dan nasional saat ini sudah mulai menjadi ancaman di berbagai negara</p>
                        </div>        
                    </div>
                </div>
        </div>
    </div>

  <!-- Footer Section End -->