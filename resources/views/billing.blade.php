<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Center - Universitas Bakrie</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/plyr.css" type="text/css">
    <link rel="stylesheet" href="/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/home">
                            <img src="/resizelogo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="home">Homepage</a></li>
                                <!-- <li><a href="/categories.html">Categories <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="./categories.html">Bisnis</a></li>
                                        <li><a href="./anime-details.html">Keuangan</a></li>
                                        <li><a href="./anime-watching.html">Teknologi</a></li>
                                        <li><a href="./blog-details.html">Politik</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li><a href="http://localhost/central-data/news-app/public/">News</a></li> -->
                                <li><a href="./profile">Profil</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="{{ route('logout') }}"><span class="fa fa-sign-out"></span>  Sign Out</a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
<section class="product spad">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="sidebar_product">
                        <div class="product__sidebar__view">
                            <div class="section-title">
                                <h5>Survey Elektabilitas Partai dan Tokoh Pilpres 2024</h5>
                            </div>
                            <p>Pandemi Covid-19 sepertinya tidak mengurangi perhatian publik terhadap isu-isu politik di tanah air. Pandemi ini justru membuat publik semakin kritis terhadap kebijakan-kebijakan pemerintah saat ini. Kepercayaan publik terhadap pemerintah mulai diuji dalam banyak hal. Di satu sisi manuver politik menjelang Pemilu 2024 kian nampak di depan mata. Tak jarang tokoh & Partai Politik pun mulai mencoba mencari simpati dan dukungan. 
                              Kami mencoba memotret bagaimana pandangan publik terhadap dinamika politik menjelang Pemilu 2024. Universitas Bakrie ingin melihat peta elektoral terkini, dengan melakukan survei dengan daring terhadap 1255 responden Laki-laki maupun Perempuan dengan rentang usia 17  tahun atau lebih, dengan margin of error--MoE sekitar ±3% pada tingkat kepercayaan 95%.</p>
                            <br><p><iframe title="Elektabilitas Partai Politik" aria-label="Bar Chart" id="datawrapper-chart-xnjyI" src="https://datawrapper.dwcdn.net/xnjyI/1/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="526"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();</script></p>
                            <br><p><iframe title="Diantara tokoh di bawah ini, siapa yang akan pilih sebagai Presiden?" aria-label="Bar Chart" id="datawrapper-chart-OoWpX" src="https://datawrapper.dwcdn.net/OoWpX/2/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="329"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();</script></p>
                            <br><p><iframe title="Pernyataan mana yang mewakili pandangan Anda?" aria-label="Pie Chart" id="datawrapper-chart-gPgAE" src="https://datawrapper.dwcdn.net/gPgAE/1/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="555"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();</script></p>
                            <br><p><iframe title="Pernyataan mana yang mewakili pandangan Anda?" aria-label="Pie Chart" id="datawrapper-chart-CSpCf" src="https://datawrapper.dwcdn.net/CSpCf/2/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="585"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();</script></p>
                            <br><p><iframe title="Apakah Anda Puas dengan Kinerja Jokowi-Maruf?" aria-label="Donut Chart" id="datawrapper-chart-uuW0u" src="https://datawrapper.dwcdn.net/uuW0u/1/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="589"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();</script></p>
                        </div>        
                    </div>
                </div>
        </div>
    </div>

  <!-- Footer Section End -->