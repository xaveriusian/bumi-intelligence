<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Center - Universitas Bakrie</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/plyr.css" type="text/css">
    <link rel="stylesheet" href="/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/home">
                            <img src="/resizelogo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="home">Homepage</a></li>
                                <!-- <li><a href="/categories.html">Categories <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="./categories.html">Bisnis</a></li>
                                        <li><a href="./anime-details.html">Keuangan</a></li>
                                        <li><a href="./anime-watching.html">Teknologi</a></li>
                                        <li><a href="./blog-details.html">Politik</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li><a href="http://localhost/central-data/news-app/public/">News</a></li> -->
                                <li><a href="./profile">Profil</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="{{ route('logout') }}"><span class="fa fa-sign-out"></span>  Sign Out</a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
<section class="product spad">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="sidebar_product">
                        <div claass="product__sidebar__view">
                            <div class="section-title">
                                <h5>Isu Penanganan Harga Minyak Goreng</h5>
                            </div>
                            <p>Kelangkaan dan kenaikan harga minyak goreng mendapatakan atensi yang cukup besar dalam ruang percakapan media sosial.

</p><br>
                            
                            <p>Dalam ruang media sosial (twitter) dapat teridentifikasi 5 klaster jejaring sosial yang mempengaruhi percakapan ruang media sosial yang berasal dari akun yang dominan:</p>
                            <p>
                                <ul>
                                <li>Klaster 1 (@oposisicerdas)</li>
                                <li>Klaster 2 (@alisyarief)</li>
                                <li>Klaster 3 (@democrazymedia)</li>
                                <li>Klaster 4 (@minang_cyber)</li>
                                <li>Klaster 5 (@mcaops)</li>
                                </ul>
                            </p>
                            <br>
                            <img src="/b8.png"></img>
                            <br>
                            <p>Secara keseluruhan percakapan pada ruang media sosial mengenai masalah minyak goreng telah mengalami dinamika mulai tanggal 31 Mei – 4 Juni 2022. Percakapan mengenai minyak goreng berada pada titik yang paling tinggi berada pada tanggal 2 Juni 2022 yang mencapai 3.826 percakapan.

Dari keseluruhan postingan tersebut, terdapat 10 individu / akun yang cukup popular dan berpengaruh dalam percakapan, diantaranya adalah @oposisicerdas @alisyarief @cnnindonesia dan @mcaops
</p>
<img src="/b9.png"></img>
</p>
<iframe title="[ Jumlah Pesan ]" aria-label="Column Chart" id="datawrapper-chart-6oj3O" src="https://datawrapper.dwcdn.net/6oj3O/1/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="291"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>
<p>Pada klaster 1, percakapan di dominasi oleh isu seputar penanganan masalah minyak goreng dengan melibatkan KASAD TNI dan upaya membangun agenda perlawanan terhadap oligarki dalam masalah minyak goreng dengan tagar #SaatnyaLawanOligarki

Isu-isu tersebut muncul dari percakapan pada rantai percakapan akun-akun @oposisicerdas dan @bostari4

</p><img src="/b10.png"></img>

<p>Klaster 2, percakapan di dominasi oleh pembicaraan mengenai kritik anggota DPR RI Komisi VII Mulyanto terhadap keterlibatan KASAD Jenderal TNI Dudung Abdurachman dalam masalah minyak goreng

Isu tersebut merujuk pada rilis media online yang memberitakan kritik anggota DPR RI kepada KASAD TNI. Isu ini dapat dilihat pada rantai percakapan akun-akun @alisyarief @joendymurtadho

</p> <img src="/b11.png"></img>
<p>Pada klaster 3, percakapan teridentifikasi cukup beragam, mulai dari kritik terhadap keterlibatan KASAD TNI, oligarki industri sawit, dan praktik permainan harga sawit

Isu-isu diatas dapat dilihat pada rantai percakapan akun-akun @democrazymedia @geloraco @tempodotco dan @pandiahendra
</p>
<img src="/b12.png"></img>
<p>Pada ruang pembicaraan media sosial tentang masalah minyak goreng, dapat diketahui beberapa kata kunci yang secara frekuensi muncul dalam pembicaraan.

Melalui kata kunci tersebut dapat diidentifikasi mengenai ide pokok dan kalimat-kalimat utama dari pembicaraan pada ruang media sosial. Kata-kata yang muncul dalam frekuensi yang cukup sering adalah; goreng, minyak, tindak, dan jateng
</p>
<img src="/b13.png"></img>

                        </div>        
                    </div>
                </div>
        </div>
    </div>

  <!-- Footer Section End -->