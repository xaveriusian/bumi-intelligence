<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Center - Universitas Bakrie</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/plyr.css" type="text/css">
    <link rel="stylesheet" href="/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/home">
                            <img src="/resizelogo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="home">Homepage</a></li>
                                <!-- <li><a href="/categories.html">Categories <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="./categories.html">Bisnis</a></li>
                                        <li><a href="./anime-details.html">Keuangan</a></li>
                                        <li><a href="./anime-watching.html">Teknologi</a></li>
                                        <li><a href="./blog-details.html">Politik</a></li>
                                    </ul>
                                </li> -->
                                <!-- <li><a href="http://localhost/central-data/news-app/public/">News</a></li> -->
                                <li><a href="./profile">Profil</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="{{ route('logout') }}"><span class="fa fa-sign-out"></span>  Sign Out</a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
<section class="product spad">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="sidebar_product">
                        <div claass="product__sidebar__view">
                            <div class="section-title">
                                <h5>JAKARTA E PRIX</h5>
                            </div>
                            <p>Event Jakarta E Prix (Formula E) mendapatakan atensi yang cukup besar dalam ruang percakapan media sosial.</p>
                            <p>Dalam ruang media sosial (twitter) dapat teridentifikasi 5 klaster jejaring sosial yang mempengaruhi percakapan ruang media sosial yang berasal dari akun yang dominan:</p>
                            <p>
                                <ul>
                                <li>Klaster 1 (@maspiyuaja)</li>
                                <li>Klaster 2 (@gunromli)</li>
                                <li>Klaster 3 (@detikcom)</li>
                                <li>Klaster 4 (@panca66)</li>
                                <li>Klaster 5 (@fia)</li>
                                </ul>
                            </p>
                            <br>
                            <div class="screen-view"><img src="/b1.png"></img></div>
                            <br>
                            <p>Secara keseluruhan jumlah postingan pada ruang media sosial mengenai Jakarta E Prix terus megalami peningkatan hingga mencapai angka dari tanggal 3-4 Juni 2022 yaitu dari angka 2.905 postingan hingga mencapai 7.095.

Dari keseluruhan postingan tersebut terdapat 10 individu / akun yang cukup popular dan berpengaruh dalam percakapan, 5 diantaranya adalah: @gunromli, @detikcom, @maspiyuaja @cnnindonesia, @dennysiregar
</p>
                          
                            <p><iframe title="[ Jumlah Postingan ] (Copy)" aria-label="Grouped Column Chart" id="datawrapper-chart-i8xJ3" src="https://datawrapper.dwcdn.net/i8xJ3/1/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="400"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>
</p>
<iframe title="[ Individu/ Akun Rujukan ]" aria-label="Range Plot" id="datawrapper-chart-FwzEJ" src="https://datawrapper.dwcdn.net/FwzEJ/2/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="268"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>
<p>Pada klaster 1, percakapan di dominasi oleh isu seputar tentang dukungan terhadap Formula E, Pawang Hujan, dan seputar isu mengenai absennya dukungan BUMN terhadap penyelenggaraan Jakarta E Prix tersebut.

Isu-isu tersebut muncul dari percakapan pada rantai percakapan akun-akun @maspiyuaja dan @satriohendri 
</p><img src="/b4.png"></img>

<p>Klaster 2, percakapan didominasi isu mengenai  isu seputar absennya BUMN dalam pendanaan Jakarta E Prix, Event Jakarta E Prix sebagai panggung politik Anies Baswedan, hingga kritik terhadap pengunaan anggaran Pemprov DKI bagi event Jakarta E Prix

Narasi-narasi isu tersebut muncul pada rantai percakapan akun-akun @gunromli @bumnbersatu dan @_ekokuntadhi
</p> <img src="/b5.png"></img>
<p>Pada klaster 3, percakapan di dominasi oleh seputar pemberitaan media mainstream yang memberikan informasi mengenai Jakarta E Prix. Informasi yang disampaikan antara lain himbauan Gubernur DKI Jakarta untuk tidak membawa atribut atau menyuarakan yel-yel politis.

Himbauan tersebut dapat dilihat pada berita-berita media seperti: @detikcom @kumparan @vivacoid dan @kompascom
</p>
<img src="/b6.png"></img>
<p>Pada ruang pembicaraan media sosial tentang Jakarta E Prix, dapat diketahui beberapa kata kunci yang secara frekuensi muncul dalam pembicaraan.

Melalui kata kunci tersebut dapat diidentifikasi mengenai ide pokok dan kalimat-kalimat utama dari pembicaraan pada ruang media sosial. Kata-kata yang muncul dalam frekuensi yang cukup sering adalah; Formula, Anies, Sponsor, dan BUMN

</p>
<img src="/b7.jpg"></img>

                        </div>        
                    </div>
                </div>
        </div>
    </div>

  <!-- Footer Section End -->