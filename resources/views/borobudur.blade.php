<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Data Center - Universitas Bakrie</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap"
    rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="/css/plyr.css" type="text/css">
    <link rel="stylesheet" href="/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="/css/style.css" type="text/css">
</head>

<header class="header">
        <div class="container">
            <div class="row">
                <div class="col-lg-2">
                    <div class="header__logo">
                        <a href="/home">
                            <img src="/resizelogo.png" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="header__nav">
                        <nav class="header__menu mobile-menu">
                            <ul>
                                <li class="active"><a href="home">Homepage</a></li>
                                <!-- <li><a href="/categories.html">Categories <span class="arrow_carrot-down"></span></a>
                                    <ul class="dropdown">
                                        <li><a href="./categories.html">Bisnis</a></li>
                                        <li><a href="./anime-details.html">Keuangan</a></li>
                                        <li><a href="./anime-watching.html">Teknologi</a></li>
                                        <li><a href="./blog-details.html">Politik</a></li>
                                    </ul>
                                </li> -->
                                
                                <li><a href="./profile">Profil</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="header__right">
                        <a href="{{ route('logout') }}"><span class="fa fa-sign-out"></span>  Sign Out</a>
                    </div>
                </div>
            </div>
            <div id="mobile-menu-wrap"></div>
        </div>
    </header>
<section class="product spad">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="sidebar_product">
                        <div claass="product__sidebar__view">
                            <div class="section-title">
                                <h5>Harga Tiket Borobudur</h5>
                            </div>
                            <p>Harga baru tiket masuk Candi Borobudur mendapatkan atensi yang cukup besar dalam ruang percakapan media sosial.

</p><br>
                            
                            <p>Dalam ruang media sosial (twitter) dapat teridentifikasi 5 klaster jejaring sosial yang mempengaruhi percakapan ruang media sosial yang berasal dari akun yang dominan:</p>
                            <p>
                                <ul>
                                <li>Klaster 1 (@infomitigasi)</li>
                                <li>Klaster 2 (@dennysiregar7)</li>
                                <li>Klaster 3 (@alisyarief)</li>
                                <li>Klaster 4 (@detikcom)</li>
                                <li>Klaster 5 (@cnnindonesia)</li>
                                </ul>
                            </p>
                            <br>
                            <img src="/b14.png"></img>
                            <br>
                            <p>Secara keseluruhan, percakapan pada ruang media sosial harga baru tiket masuk Candi Borobudur sebesar 750 ribu rupiah bagi turis lokal telah mengalami peningkatan cukup siginifikan pada tanggal 4-5 Juni 2022. Tercatat pada tangggal 5 Juni, postingan telah mencapai 9.365 postingan setelah sebelumnya pada tanggal 4 Juni hanya berjumlah 635 postingan.

Dari keseluruhan postingan tersebut, terdapat 10 individu / akun yang cukup popular dan berpengaruh dalam percakapan, diantaranya adalah; @infomitigasi, @detikcom, @cnnindonesia, @dennysiregar7, @voidotid

</p>
<iframe title="[Individu / Akun Rujukan]" aria-label="Arrow Plot" id="datawrapper-chart-Ys8wX" src="https://datawrapper.dwcdn.net/Ys8wX/1/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="268"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>
</p>
<iframe title="" aria-label="Donut Chart" id="datawrapper-chart-NqHf4" src="https://datawrapper.dwcdn.net/NqHf4/1/" scrolling="no" frameborder="0" style="width: 0; min-width: 100% !important; border: none;" height="527"></iframe><script type="text/javascript">!function(){"use strict";window.addEventListener("message",(function(e){if(void 0!==e.data["datawrapper-height"]){var t=document.querySelectorAll("iframe");for(var a in e.data["datawrapper-height"])for(var r=0;r<t.length;r++){if(t[r].contentWindow===e.source)t[r].style.height=e.data["datawrapper-height"][a]+"px"}}}))}();
</script>
<p>Pada klaster 1, percakapan pada media sosial didominasi oleh isu seputar harga tiket yang mahal sehingga berpotensi membatasi generasi muda untuk mengenal sejarah dan budaya.

Akun @infomitigasi teridentifikasi sebagai akun popular sebagai rujukan 


</p><img src="/b15.png"></img>

<p>Klaster 2, percakapan tersegrasi menjadi 2 isu; dukungan terhadap dan penolakan terhadap kenaikan harga tiket Candi Borobudur

Isu dukungan terhadap kenaikan harga tiket Candi Borubudur menjadi isu dari individu / akun rujukan @dennysiregar7 dengan narasi perlindungan cagar budaya. Isu penolakan kenaikan harga tiket Candi Borobudur berasal dari individu / akun rujukan @lucknuut dengan narasi dampak terhadap UMKM dan warga lokal

</p> <img src="/b16.png"></img>
<p>Klaster 3, percakapan pada media sosial didominasi oleh permintaan kajian Pemerintah terhadap kenaikan harga tiket Candi Borobudur. 

Individu / akun rujukan yang teridentifikasi adalah akun @sandiuno, @jokowi, @ganjarpranowo. Ketiga akun tersebut banyak mendapatkan konfirmasi tentang kenaikan harga tiket Candi Borobudur 

</p>
<img src="/b17.png"></img>
<p>Pada ruang pembicaraan media sosial tentang harga tiket Candi Borobudur, dapat diketahui beberapa kata kunci yang secara frekuensi muncul dalam pembicaraan.

Melalui kata kunci tersebut dapat diidentifikasi mengenai ide pokok dan kalimat-kalimat utama dari pembicaraan pada ruang media sosial. Kata-kata yang muncul dalam frekuensi yang cukup sering adalah; tiket, harga, 750k, candi, Borobudur, membatasi

</p>
<img src="/b18.png"></img>

                        </div>        
                    </div>
                </div>
        </div>
    </div>

  <!-- Footer Section End -->